Name: Quentin VEYRET
ID: 13316590

NOTES:
* to init the P2P system, type "java -jar P2P_Client.jar --boot NODE_ID", with NODE_ID the ID of the first node
* to connect to a network, type "java -jar P2P_Client.jat --bootstrap IP_BOOTSTRAP --id NODE_ID --target_id BOOTSTRAP_ID"
with IP_BOOTSTRAP the ip address of the boostrap node, NODE_ID the ID of the new node and BOOTSTRAP_ID the id of the bootstrap node.
note that before joining a network, you must have started it before

* to configure the port on which the nodes will listen, modify the "node.properties" file for each file (but be careful of keeping the same port for every node, 8767 by default)
* to index a new page, type "I URL WORDS", with URL the url of the page to index and WORDS the list of words to be indexed for this page, separated by space characters
* to leave a network properly, type "Q" or "q"
* to see your current routing table, type "R" or "r"
* to search words, type "S" or "s" followed by the words to search separated by space characters