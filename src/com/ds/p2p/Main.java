package com.ds.p2p;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.util.NodeConfig;

public class Main {

	private static PeerSearchSimplified peer = new PeerSearch();

	private static boolean initNetwork = false;
	private static boolean joinNetwork = false;
		private static String bootstrapIp;
		private static String id;
		private static String target_id;
		private static long networkId;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (computeArgs(args)) {
			if (initNetwork || joinNetwork) {
				try {
					peer.init(new DatagramSocket(NodeConfig.getPort()));
				} catch (SocketException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}
			if (joinNetwork) {
				networkId = peer.joinNetwork(new InetSocketAddress(bootstrapIp, NodeConfig.getPort()), id, target_id);
			}
			while (true) {
				computeUserInput();
			}
		}
		else {
			System.out.println("You must start the program with the options \"--boot nodeId\" or \"--bootstrap ipBootstrapNode --id nodeId --target_id targetNodeId\"");
			System.exit(0);
		}
	}

	private static boolean computeArgs(String[] args) {
		if (args.length == 0) {
			return false;
		}
		String argsCLI = "";
		for (String arg : args) {
			argsCLI += arg + " ";
		}
		StringTokenizer st = new StringTokenizer(argsCLI, "--");
		while (st.hasMoreTokens()) {
			String s = st.nextToken();
			StringTokenizer stArg = new StringTokenizer(s, " ");
			String paramType = stArg.nextToken();
			if (paramType.equals("boot")) {
				initNetwork = true;
				if (stArg.hasMoreTokens()) {
					NodeConfig.setNode_id(Integer.parseInt(stArg.nextToken()));
				} else { return false; }
			}
			else if (paramType.equals("bootstrap")) {
				joinNetwork = true;
				if (stArg.hasMoreTokens()) {
					bootstrapIp = stArg.nextToken();
				} else { return false; }
			}
			else if (paramType.equals("id")) {
				joinNetwork = true;
				if (stArg.hasMoreTokens()) {
					id = stArg.nextToken();
				} else { return false; }
			}
			else if(paramType.equals("target_id")) {
				joinNetwork = true;
				if (stArg.hasMoreTokens()) {
					target_id = stArg.nextToken();
				} else { return false; }
			}
		}
		return true;
	}

	public static void computeUserInput() {
		System.out.println("Enter your command:");
		Scanner in = new Scanner(System.in);
		String read = in.next();

		System.out.println(read);
		if (read.toLowerCase().equals("q")) {
			peer.leaveNetwork(networkId);
			System.exit(0);
		}
		else if (read.toLowerCase().equals("r")) {
			List<Node> routingTable = SearchData.getRoutingTable();
			if (routingTable.size() == 0) {
				System.out.println("Empty routing table");
			}
			for (Node n : routingTable) {
				System.out.println("- " + n.getNode_id() + ": " + n.getIp_address());
			}
		}
		else if (read.toLowerCase().equals("i")) {
			String pageToIndex = in.next();
			System.out.println("You want to index \"" + pageToIndex + "\"");
			// the rest of the command line are the words to index
			List<String> words = new ArrayList<String>();
			StringTokenizer wordsRead = new StringTokenizer(in.nextLine());
			while (wordsRead.hasMoreTokens()) {
				words.add(wordsRead.nextToken());
			}
			peer.indexPage(pageToIndex, words.toArray(new String[0]));
		}
		else if (read.toLowerCase().equals("s")) {
			StringTokenizer st = new StringTokenizer(in.nextLine());
			String[] searchedWords = new String[st.countTokens()];
			int i = 0;
			while (st.hasMoreTokens()) {
				searchedWords[i] = st.nextToken();
				i++;
			}
			peer.search(searchedWords);
		}
	}

}
