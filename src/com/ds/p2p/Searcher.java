package com.ds.p2p;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.ds.p2p.messages.MessSearch;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.Hash;
import com.ds.p2p.util.NodeConfig;

public class Searcher {

	private String[] words;
	private int sentRequests;
	private ExecutorService servicePool;
	private List<SearchResult> results = new ArrayList<SearchResult>();

	public Searcher(String[] words) {
		this.words = words;
	}

	public SearchResult[] search() {
		ExecutorService service = Executors.newFixedThreadPool(words.length);
		for (String word : words) {
			if (SearchData.containsWord(word)) {
				for (SearchResult sr : SearchData.search(new String[] {word})) {
					results.add(sr);
				}
			}
			else {
				sentRequests++;
				int hashedWord = Hash.hashCode(word);
				Node closestNode = SearchData.getNearestNode(hashedWord);
				InetSocketAddress inetSA = new InetSocketAddress(closestNode.getIp_address(), NodeConfig.getPort());
				P2PMessage message = new MessSearch(word, hashedWord, NodeConfig.getNode_id());
				UDPSender sender = new UDPSender(message, inetSA);
				service.execute(sender);
			}
		}
		servicePool = Executors.newFixedThreadPool(1);
		servicePool.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {}
			}
		});
		// wait until we receive all the responses
		while (!servicePool.isShutdown()) {}
		return results.toArray(new SearchResult[0]);
	}

	public synchronized void answerRequest(SearchResult[] results) {
		System.out.println("response received");
		for (SearchResult result : results) {
			this.results.add(result);
		}
		sentRequests--;
		if (sentRequests == 0) {
			servicePool.shutdown();
		}
	}
}
