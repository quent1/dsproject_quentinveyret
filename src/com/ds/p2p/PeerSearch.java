package com.ds.p2p;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.ds.p2p.messages.MessIndex;
import com.ds.p2p.messages.MessJoiningNetwork;
import com.ds.p2p.messages.MessLeavingNetwork;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.impl.CheckAckIndexReceived;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.network.UDPServer;
import com.ds.p2p.util.Hash;
import com.ds.p2p.util.NodeConfig;

public class PeerSearch implements PeerSearchSimplified {

	public static long network_id = 0;
	/**
	 * Corresponds to the pending INDEX messages sent (for which we are waiting an ACK_INDEX message)
	 * It is stored by the pair <word, nodeId>
	 */
	public static Map<String, Integer> pendingIndex = new HashMap<String, Integer>();
	public static List<Integer> pingedNodes = new ArrayList<Integer>();
	public static Searcher currentSearch;

	@Override
	public void init(DatagramSocket udp_socket) {
		// we start the listening UDP server
		UDPServer server = null;
		try {
			server = new UDPServer(udp_socket);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		server.start();
	}

	@Override
	public long joinNetwork(InetSocketAddress bootstrap_node,
			String identifier, String target_identifier) {
		NodeConfig.setNode_id(Integer.parseInt(identifier));
		// the first entry in our routing table is the bootstrap node
		SearchData.insertNode(new Node(Integer.parseInt(target_identifier), bootstrap_node.getHostName(), bootstrap_node.getPort()));

		// we indicate the server that we want to join it (the listening UDP server is supposed to be started with "init" to receive the response)
		P2PMessage message = new MessJoiningNetwork(Integer.parseInt(identifier)
													, Integer.parseInt(target_identifier)
													, NodeConfig.getIpAddress());
		UDPSender sender = new UDPSender(message, bootstrap_node);
		sender.start();
		return PeerSearch.network_id++;
	}

	@Override
	public boolean leaveNetwork(long network_id) {
		// NOTE: network_id is not used yet
		P2PMessage mess = new MessLeavingNetwork(NodeConfig.getNode_id());
		int routingTableSize = SearchData.getRoutingTable().size();
		if (routingTableSize > 0) {
			ExecutorService service = Executors.newFixedThreadPool(routingTableSize);
			for (Node node : SearchData.getRoutingTable()) {
				InetSocketAddress inetSA = new InetSocketAddress(node.getIp_address(), node.getPort());
				UDPSender sender = new UDPSender(mess, inetSA);
				service.execute(sender);
			}
			try {
				return service.awaitTermination(2, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@Override
	public void indexPage(String url, String[] unique_words) {
		Map<P2PMessage, Node> messages_targetIp = new HashMap<P2PMessage, Node>();
		for (String word : unique_words) {
			int hashedWord = Hash.hashCode(word);
			// if the word is supposed to be stored on the current node, we store it
			if (SearchData.isCurrentNodeNearestNode(hashedWord)) {
				System.out.println("\"" + word + "\" indexed locally");
				SearchData.indexWordPage(url, word);
			}
			else {
				List<String> urls = new ArrayList<String>();
				urls.add(url);
				messages_targetIp.put(new MessIndex(hashedWord, NodeConfig.getNode_id(), word, urls)
										, SearchData.getNearestNode(hashedWord));
			}
		}
		if (!messages_targetIp.isEmpty()) { // we need to send INDEX messages to the corresponding closer nodes
			ExecutorService service = Executors.newFixedThreadPool(messages_targetIp.size());
			for (P2PMessage message : messages_targetIp.keySet()) {
				InetSocketAddress inetSA = new InetSocketAddress(messages_targetIp.get(message).getIp_address(), messages_targetIp.get(message).getPort());
				UDPSender sender = new UDPSender(message, inetSA);
				PeerSearch.pendingIndex.put(((MessIndex)message).getKeyword(), ((MessIndex)message).getTarget_id());
				service.execute(sender);
				ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
				// in 30 seconds, we will check that we have received the response for the keyword
				scheduler.schedule(new CheckAckIndexReceived(((MessIndex)message).getKeyword(), ((MessIndex)message).getTarget_id(), inetSA), 30, TimeUnit.SECONDS);
			}
		}
	}

	@Override
	public SearchResult[] search(String[] words) {
		currentSearch = new Searcher(words);
		return currentSearch.search();
	}
}
