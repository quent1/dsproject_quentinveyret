package com.ds.p2p.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Corresponds to the current node configuration
 * @author Quentin
 *
 */
public class NodeConfig {

	static private int node_id;

	static public String getIpAddress() {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("node.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty("ip_address");
	}

	static public int getPort() {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("node.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Integer.parseInt(prop.getProperty("port"));
	}

	public static int getNode_id() {
		return node_id;
	}
	public static void setNode_id(int node_id) {
		NodeConfig.node_id = node_id;
	}
}
