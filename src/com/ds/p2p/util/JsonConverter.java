package com.ds.p2p.util;

import com.ds.p2p.messages.MessAck;
import com.ds.p2p.messages.MessAckIndex;
import com.ds.p2p.messages.MessIndex;
import com.ds.p2p.messages.MessJoiningNetwork;
import com.ds.p2p.messages.MessJoiningNetworkRelay;
import com.ds.p2p.messages.MessLeavingNetwork;
import com.ds.p2p.messages.MessPing;
import com.ds.p2p.messages.MessRoutingInfo;
import com.ds.p2p.messages.MessSearch;
import com.ds.p2p.messages.MessSearchResponse;
import com.ds.p2p.messages.commons.P2PMessage;
import com.google.gson.Gson;

public class JsonConverter {
	static public String convert(P2PMessage message) {
		Gson gson = new Gson();
		return gson.toJson(message);
	}

	static public P2PMessage convert(String json) {
		Gson gson = new Gson();
		// we parse with an arbitrary type, to retrieve the message real type and parse it with the right type
		P2PMessage mess = gson.fromJson(json, MessAck.class);
		switch (mess.getType()) {
		case INDEX:
			return gson.fromJson(json, MessIndex.class);
		case JOINING_NETWORK_RELAY_SIMPLIFIED:
			return gson.fromJson(json, MessJoiningNetworkRelay.class);
		case JOINING_NETWORK_SIMPLIFIED:
			return gson.fromJson(json, MessJoiningNetwork.class);
		case LEAVING_NETWORK:
			return gson.fromJson(json, MessLeavingNetwork.class);
		case PING:
			return gson.fromJson(json, MessPing.class);
		case ROUTING_INFO:
			return gson.fromJson(json, MessRoutingInfo.class);
		case SEARCH:
			return gson.fromJson(json, MessSearch.class);
		case SEARCH_RESPONSE:
			return gson.fromJson(json, MessSearchResponse.class);
		case ACK_INDEX:
			return gson.fromJson(json, MessAckIndex.class);
		default: // ACK
			return mess;
		}
	}
}
