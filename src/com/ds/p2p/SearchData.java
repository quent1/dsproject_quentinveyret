package com.ds.p2p;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.util.NodeConfig;

/**
 * Contains all the local data stored on the node
 * @author Quentin
 *
 */
public class SearchData {

	private static SortedMap<Integer, Node> routingTable = /*Collections.synchronizedSortedMap(*/new TreeMap<Integer, Node>()/*)*/;
	private static SortedMap<String, List<String>> pageIndex = new TreeMap<String, List<String>>();
	private static SortedMap<String, List<String>> wordsIndex = new TreeMap<String, List<String>>();

	/**
	 * 
	 * @param nodeId
	 * @return <code>false</code> if we have in the routing table a node which is closer to the targeted node ID, <code>true</code> else.
	 */
	static public boolean isCurrentNodeNearestNode(int nodeId) {
		Node nearestNode = getNearestNode(nodeId);
		if (nearestNode == null
				|| Math.abs(nodeId - NodeConfig.getNode_id())
				<= Math.abs(nodeId - nearestNode.getNode_id())) {
			return true;
		}
		return false;
	}

	static public void insertNode(Node node) {
		routingTable.put(node.getNode_id(), node);
	}
	static public void insertRoutingTable(List<Node> routing_table) {
		for (Node n : routing_table) {
			insertNode(n);
		}
	}

	static public void removeNode(int nodeId) {
		if (routingTable.containsKey(nodeId)) {
			routingTable.remove(nodeId);
		}
	}

	static public List<Node> getRoutingTable() {
		List<Node> ret = new ArrayList<Node>(routingTable.values());
		return ret;
	}

	static public String getIpAddress(int nodeId) {
		return routingTable.get(nodeId).getIp_address();
	}

	static public Node getNearestNode(int nodeId) {
		Entry<Integer, Node> entry = ((TreeMap<Integer, Node>)routingTable).ceilingEntry(nodeId);
		if (entry == null) {
			entry = ((TreeMap<Integer, Node>)routingTable).floorEntry(nodeId);
		}
		if (entry == null) {
			return null;
		}
		return entry.getValue();
	}

	static private void insertPage(String url, String word) {
		List<String> words = pageIndex.get(url);
		if (words != null) {
			words.add(word);
		}
		else {
			words = new ArrayList<String>();
			words.add(word);
			pageIndex.put(url, words);
		}
	}
	static private void insertWord(String url, String word) {
		List<String> urls = wordsIndex.get(word);
		if (urls != null) {
			urls.add(url);
		}
		else {
			urls = new ArrayList<String>();
			urls.add(url);
			wordsIndex.put(word, urls);
		}
	}
	static public void indexWordPage(String url, String word) {
		insertPage(url, word);
		insertWord(url, word);
	}

	/**
	 * Local search
	 * @param words
	 * @return
	 */
	static public SearchResult[] search(String[] words) {
		// contains the URLs and their list of matched words in the list
		Map<String, List<String>> results = new HashMap<String, List<String>>();

		// for each word, find the URLs, and add them to the result map with the word added to their matching list
		for (String word : words) {
			List<String> urls = wordsIndex.get(word);
			if (urls != null) {
				for (String url : urls) {
					// add the word to the matching words for the URL
					List<String> wordsUrl = results.get(url);
					if (wordsUrl != null) {
						wordsUrl.add(word);
					}
					else {
						// if this is the first word matching the URL, create the entry
						wordsUrl = new ArrayList<String>();
						wordsUrl.add(word);
						results.put(url, wordsUrl);
					}
				}
			}
		}

		List<SearchResult> resultList = new ArrayList<SearchResult>();
		for (Entry<String, List<String>> entry : results.entrySet()) {
			List<String> values = entry.getValue();
			// convert the List into an array of String
			String[] valuesString = values.toArray(new String[0]);

			// create the SearchResult object
			resultList.add(new SearchResult(valuesString
											, entry.getKey()
											, pageIndex.get(entry.getKey()).size()));
		}
		return resultList.toArray(new SearchResult[0]);
	}

	static public boolean containsWord(String word) {
		return wordsIndex.containsKey(word);
	}
}
