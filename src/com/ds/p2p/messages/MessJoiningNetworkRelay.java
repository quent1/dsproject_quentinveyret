package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.JoiningNetworkRelayMessProc;

public class MessJoiningNetworkRelay extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', indicating the ID of the joining node
	 */
	private int node_id;
	/**
	 * A non-negative number of order 2'^32^', indicating the target node for this message
	 */
	private int target_id;
	/**
	 * A non-negative number of order 2'^32^', of the gateway node
	 */
	private int gateway_id;

	public MessJoiningNetworkRelay() {
		type = MessageType.JOINING_NETWORK_RELAY_SIMPLIFIED;
	}
	public MessJoiningNetworkRelay(int node_id, int target_id, int gateway_id) {
		this();
		this.node_id = node_id;
		this.target_id = target_id;
		this.gateway_id = gateway_id;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new JoiningNetworkRelayMessProc(this);
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public int getTarget_id() {
		return target_id;
	}
	public void setTarget_id(int target_id) {
		this.target_id = target_id;
	}
	public int getGateway_id() {
		return gateway_id;
	}
	public void setGateway_id(int gateway_id) {
		this.gateway_id = gateway_id;
	}
}
