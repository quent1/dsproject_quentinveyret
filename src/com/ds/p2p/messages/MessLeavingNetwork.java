package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.LeavingNetworkMessProc;

public class MessLeavingNetwork extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', identifying the leaving node
	 */
	private int node_id;

	public MessLeavingNetwork() {
		type = MessageType.LEAVING_NETWORK;
	}
	public MessLeavingNetwork(int node_id) {
		this();
		this.node_id = node_id;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new LeavingNetworkMessProc(this);
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
}
