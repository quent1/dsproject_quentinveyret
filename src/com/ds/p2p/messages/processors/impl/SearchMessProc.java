package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import com.ds.p2p.SearchData;
import com.ds.p2p.SearchResult;
import com.ds.p2p.messages.MessSearch;
import com.ds.p2p.messages.MessSearchResponse;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.URLResponse;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.NodeConfig;

public class SearchMessProc extends MessageProcessor {

	private MessSearch message;

	public SearchMessProc(MessSearch mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("SearchMessProc");
		// if we are the closest known node for this word, we search in our index
		if (SearchData.isCurrentNodeNearestNode(message.getNode_id())) {
			String word = message.getWord();
			SearchResult[] results;
			if (SearchData.containsWord(word)) {
				results = SearchData.search(new String[] {word});
			}
			else {
				results = new SearchResult[0];
			}
			List<URLResponse> response = new ArrayList<URLResponse>();
			for (SearchResult result : results) {
				// TODO: rank = 0 is temporary, to be improved
				response.add(new URLResponse(result.getUrl(), 0));
			}
			MessSearchResponse mess = new MessSearchResponse(word, message.getSender_id(), NodeConfig.getNode_id(), response);
			InetSocketAddress inetSA = new InetSocketAddress(ipSender, NodeConfig.getPort());
			UDPSender sender = new UDPSender(mess, inetSA);
			sender.start();
		}
		else { // we know a node which is closer for this word, so we forward the message to it
			Node closerNode = SearchData.getNearestNode(message.getNode_id());
			InetSocketAddress inetSA = new InetSocketAddress(closerNode.getIp_address(), NodeConfig.getPort());
			UDPSender sender = new UDPSender(message, inetSA);
			sender.start();
		}
	}

	public MessSearch getMessage() {
		return message;
	}
	public void setMessage(MessSearch message) {
		this.message = message;
	}
}
