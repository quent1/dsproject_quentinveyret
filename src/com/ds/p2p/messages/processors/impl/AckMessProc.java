package com.ds.p2p.messages.processors.impl;

import com.ds.p2p.PeerSearch;
import com.ds.p2p.messages.MessAck;
import com.ds.p2p.messages.processors.MessageProcessor;

public class AckMessProc extends MessageProcessor {

	private MessAck message;

	public AckMessProc(MessAck mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("AckMessProc");
		PeerSearch.pingedNodes.remove(Integer.valueOf(message.getNode_id()));
	}

	public MessAck getMessage() {
		return message;
	}
	public void setMessage(MessAck message) {
		this.message = message;
	}
}
