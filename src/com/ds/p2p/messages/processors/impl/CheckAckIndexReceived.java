package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.ds.p2p.PeerSearch;
import com.ds.p2p.messages.MessPing;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.NodeConfig;

/**
 * This class is used to check that a the ACK_INDEX has been receive for an INDEX message
 * @author Quentin
 *
 */
public class CheckAckIndexReceived implements Runnable {

	private String keyword;
	private int targetId;
	private InetSocketAddress target;

	public CheckAckIndexReceived(String keyword, int targetId, InetSocketAddress target) {
		this.keyword = keyword;
		this.targetId = targetId;
		this.target = target;
	}

	@Override
	public void run() {
		// if the response hasn't been received, we need to send a PING request to check that the node is still alive
		if (PeerSearch.pendingIndex.containsKey(keyword)) {
			PeerSearch.pendingIndex.remove(keyword);
			System.out.println("ACK_INDEX message not received for word " + keyword);
			PeerSearch.pingedNodes.add(targetId);
			P2PMessage message = new MessPing(targetId, NodeConfig.getNode_id(), NodeConfig.getIpAddress());
			UDPSender sender = new UDPSender(message, target);
			sender.start();
			ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
			scheduler.schedule(new CheckAckReceived(targetId), 10, TimeUnit.SECONDS);
		}
	}
}
