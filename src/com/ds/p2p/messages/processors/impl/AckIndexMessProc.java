package com.ds.p2p.messages.processors.impl;

import com.ds.p2p.PeerSearch;
import com.ds.p2p.messages.MessAckIndex;
import com.ds.p2p.messages.processors.MessageProcessor;

public class AckIndexMessProc extends MessageProcessor {

	private MessAckIndex message;

	public AckIndexMessProc(MessAckIndex mess) {
		this.message = mess;
	}
	@Override
	public void run() {
		System.out.println("AckIndexMessProc");
		PeerSearch.pendingIndex.remove(message.getKeyword());
	}

	public MessAckIndex getMessage() {
		return message;
	}
	public void setMessage(MessAckIndex message) {
		this.message = message;
	}

}
