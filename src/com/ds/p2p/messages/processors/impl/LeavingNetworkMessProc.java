package com.ds.p2p.messages.processors.impl;

import com.ds.p2p.SearchData;
import com.ds.p2p.messages.MessLeavingNetwork;
import com.ds.p2p.messages.processors.MessageProcessor;

public class LeavingNetworkMessProc extends MessageProcessor {

	private MessLeavingNetwork message;

	public LeavingNetworkMessProc(MessLeavingNetwork mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("LeavingNetworkMessProc: " + message.getNode_id());
		// remove the node from our routing table if it was present
		SearchData.removeNode(message.getNode_id());
	}

	public MessLeavingNetwork getMessage() {
		return message;
	}
	public void setMessage(MessLeavingNetwork message) {
		this.message = message;
	}

}
