package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;

import com.ds.p2p.messages.MessAck;
import com.ds.p2p.messages.MessPing;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.NodeConfig;

public class PingMessProc extends MessageProcessor {

	private MessPing message;

	public PingMessProc(MessPing mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("PingMessProc");
		// we answer with an ACK message
		P2PMessage mess = new MessAck(NodeConfig.getNode_id(), NodeConfig.getIpAddress());
		UDPSender sender = new UDPSender(mess, new InetSocketAddress(ipSender, portSender));
		sender.start();
	}

	public MessPing getMessage() {
		return message;
	}
	public void setMessage(MessPing message) {
		this.message = message;
	}

}
