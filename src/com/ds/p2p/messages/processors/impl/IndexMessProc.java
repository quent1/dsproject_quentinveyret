package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;

import com.ds.p2p.SearchData;
import com.ds.p2p.messages.MessAckIndex;
import com.ds.p2p.messages.MessIndex;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.Hash;

public class IndexMessProc extends MessageProcessor {

	private MessIndex message;

	public IndexMessProc(MessIndex mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("IndexMessProc: \"" + message.getKeyword() + "\" from " + message.getSender_id() + ":");
		for (String url : message.getLink()) {
			System.out.println("- " + url);
		}
		int hashedWord = Hash.hashCode(message.getKeyword());
		// if we are the closest node (in our routing table), we index the word and the associated pages
		if (SearchData.isCurrentNodeNearestNode(hashedWord)) {
			System.out.println("Indexed locally");
			for (String url : message.getLink()) {
				SearchData.indexWordPage(url, message.getKeyword());
			}
			// send the ACK_INDEX message to the node closest to the target node
			P2PMessage mess = new MessAckIndex(message.getSender_id(), message.getKeyword());
			UDPSender sender = new UDPSender(mess, new InetSocketAddress(
													SearchData.getNearestNode(message.getSender_id()).getIp_address()
													, portSender));
			sender.start();
		}
		else { // we need to forward the message to a closer node
			Node closerNode = SearchData.getNearestNode(hashedWord);
			message.setTarget_id(closerNode.getNode_id());
			System.out.println("Message forwarded to node " + closerNode.getNode_id() + "(" + closerNode.getIp_address() + ":" + closerNode.getPort() + ")");
			UDPSender sender = new UDPSender(message, new InetSocketAddress(closerNode.getIp_address(), closerNode.getPort()));
			sender.start();
		}
	}

	public MessIndex getMessage() {
		return message;
	}
	public void setMessage(MessIndex message) {
		this.message = message;
	}

}
