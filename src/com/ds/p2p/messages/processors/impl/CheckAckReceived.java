package com.ds.p2p.messages.processors.impl;

import com.ds.p2p.PeerSearch;
import com.ds.p2p.SearchData;

public class CheckAckReceived implements Runnable {

	private int nodeId;

	public CheckAckReceived(int nodeId) {
		this.nodeId = nodeId;
	}

	@Override
	public void run() {
		// ACK message not received, we consider that the node is dead: we update our routing table
		if (PeerSearch.pingedNodes.contains(nodeId)) {
			System.out.println("ACK message not received from node " + nodeId);
			SearchData.removeNode(nodeId);
		}
	}

}
