package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;

import com.ds.p2p.SearchData;
import com.ds.p2p.messages.MessJoiningNetwork;
import com.ds.p2p.messages.MessRoutingInfo;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.NodeConfig;

public class JoiningNetworkMessProc extends MessageProcessor {

	private MessJoiningNetwork message;

	public JoiningNetworkMessProc(MessJoiningNetwork mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("JoiningNetworkMessProc: " + message.getNode_id() + " / " + message.getIp_address());
		// first, we save the sending node in our routing table
		SearchData.insertNode(new Node(message.getNode_id(), ipSender, portSender));
		// then, because it is the simplified version, we won't send it to a relay node, we assume we are the target node
		// so we just reply with our routing table
		P2PMessage mess = new MessRoutingInfo(NodeConfig.getNode_id()
											, message.getNode_id()
											, NodeConfig.getIpAddress()
											, SearchData.getRoutingTable());
		UDPSender sender = new UDPSender(mess, new InetSocketAddress(ipSender, portSender));
		sender.start();
	}

	public MessJoiningNetwork getMessage() {
		return message;
	}
	public void setMessage(MessJoiningNetwork message) {
		this.message = message;
	}

}
