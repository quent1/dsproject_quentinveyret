package com.ds.p2p.messages.processors.impl;

import com.ds.p2p.SearchData;
import com.ds.p2p.messages.MessRoutingInfo;
import com.ds.p2p.messages.processors.MessageProcessor;

public class RoutingInfoMessPric extends MessageProcessor {

	private MessRoutingInfo message;

	public RoutingInfoMessPric(MessRoutingInfo mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("RoutingInfoMessProc");
		SearchData.insertRoutingTable(message.getRoute_table());
	}

	public MessRoutingInfo getMessage() {
		return message;
	}
	public void setMessage(MessRoutingInfo message) {
		this.message = message;
	}

}
