package com.ds.p2p.messages.processors.impl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.ds.p2p.PeerSearch;
import com.ds.p2p.SearchData;
import com.ds.p2p.SearchResult;
import com.ds.p2p.messages.MessSearchResponse;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.URLResponse;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.network.UDPSender;
import com.ds.p2p.util.NodeConfig;

public class SearchResponseMessProc extends MessageProcessor {

	private MessSearchResponse message;

	public SearchResponseMessProc(MessSearchResponse mess) {
		this.message = mess;
	}

	@Override
	public void run() {
		System.out.println("SearchResponseMessProc");
		// if we are the targeted node for this message, we process it
		if (message.getNode_id() == NodeConfig.getNode_id()) {
			Map<String, List<String>> results = new HashMap<String, List<String>>();
			String word = message.getWord();
			for (URLResponse resp : message.getResponse()) {
				String url = resp.getUrl();
				if (results.containsKey(url)) {
					List<String> res = results.get(url);
					res.add(word);
				}
				else {
					List<String> res = new ArrayList<String>();
					res.add(word);
					results.put(url, res);
				}
			}
			SearchResult[] resultsArray = new SearchResult[results.size()];
			int i = 0;
			for (Entry<String, List<String>> entry : results.entrySet()) {
				// TODO: frequency = 0 is temporary, to be improved
				resultsArray[i] = new SearchResult(entry.getValue().toArray(new String[0]), entry.getKey(), 0);
			}
			PeerSearch.currentSearch.answerRequest(resultsArray);
		}
		else { // if we're not the target of this message, we forward it to a closer node
			Node closerNode = SearchData.getNearestNode(message.getNode_id());
			UDPSender sender = new UDPSender(message, new InetSocketAddress(closerNode.getIp_address(), NodeConfig.getPort()));
			sender.start();
		}
	}

	public MessSearchResponse getMessage() {
		return message;
	}
	public void setMessage(MessSearchResponse message) {
		this.message = message;
	}
}
