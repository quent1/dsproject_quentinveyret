package com.ds.p2p.messages.processors;

public abstract class MessageProcessor extends Thread {

	protected String ipSender;
	protected int portSender;

	@Override
	public abstract void run();
	public void process(String ipSender, int portSender) {
		this.ipSender = ipSender;
		this.portSender = portSender;
		this.start();
	}
}
