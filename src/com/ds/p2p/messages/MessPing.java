package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.PingMessProc;

public class MessPing extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', identifying the suspected dead node
	 */
	private int target_id;
	/**
	 * A non-negative number of order 2'^32^', identifying the originator of the ping (does not change)
	 */
	private int sender_id;
	/**
	 * The ip address of node sending the message (changes each node)
	 */
	private String ip_address;

	public MessPing() {
		type = MessageType.PING;
	}
	public MessPing(int target_id, int sender_id, String ip_address) {
		this();
		this.target_id = target_id;
		this.sender_id = sender_id;
		this.ip_address = ip_address;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new PingMessProc(this);
	}

	public int getTarget_id() {
		return target_id;
	}
	public void setTarget_id(int target_id) {
		this.target_id = target_id;
	}
	public int getSender_id() {
		return sender_id;
	}
	public void setSender_id(int sender_id) {
		this.sender_id = sender_id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
}
