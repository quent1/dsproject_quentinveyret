package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.JoiningNetworkMessProc;

public class MessJoiningNetwork extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', indicating the id of the joining node
	 */
	private int node_id;
	/**
	 * A non-negative number of order 2'^32^', indicating the target noe for this message
	 */
	private int target_id;
	/**
	 * The ip address of the joining node
	 */
	private String ip_address;

	public MessJoiningNetwork() {
		type = MessageType.JOINING_NETWORK_SIMPLIFIED;
	}
	public MessJoiningNetwork(int node_id, int target_id, String ip_address) {
		this();
		this.node_id = node_id;
		this.target_id = target_id;
		this.ip_address = ip_address;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new JoiningNetworkMessProc(this);
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public int getTarget_id() {
		return target_id;
	}
	public void setTarget_id(int target_id) {
		this.target_id = target_id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
}
