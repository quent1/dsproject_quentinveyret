package com.ds.p2p.messages;

import java.util.ArrayList;
import java.util.List;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.Node;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.RoutingInfoMessPric;

public class MessRoutingInfo extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', of the gateway node
	 */
	private int gateway_id;
	/**
	 * A non-negative number of order 2'^32^', indicating the target node (and also the id of the joining node)
	 */
	private int node_id;
	/**
	 * The ip address of the node sending the routing information
	 */
	private String ip_address;
	private List<Node> route_table = new ArrayList<Node>();

	public MessRoutingInfo() {
		type = MessageType.ROUTING_INFO;
	}
	public MessRoutingInfo(int gateway_id, int node_id, String ip_address, List<Node> route_table) {
		this();
		this.gateway_id = gateway_id;
		this.node_id = node_id;
		this.ip_address = ip_address;
		this.route_table = route_table;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new RoutingInfoMessPric(this);
	}

	public int getGateway_id() {
		return gateway_id;
	}
	public void setGateway_id(int gateway_id) {
		this.gateway_id = gateway_id;
	}
	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public List<Node> getRoute_table() {
		return route_table;
	}
	public void setRoute_table(List<Node> route_table) {
		this.route_table = route_table;
	}
}
