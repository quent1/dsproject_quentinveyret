package com.ds.p2p.messages;

import java.util.ArrayList;
import java.util.List;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.commons.URLResponse;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.SearchResponseMessProc;

public class MessSearchResponse extends P2PMessage {

	/**
	 * The word to search for
	 */
	private String word;
	/**
	 * Target node ID
	 */
	private int node_id;
	/**
	 * A non-negative number of order 2'^32^', of this message originator
	 */
	private int sender_id;
	private List<URLResponse> response = new ArrayList<URLResponse>();

	public MessSearchResponse() {
		type = MessageType.SEARCH_RESPONSE;
	}
	public MessSearchResponse(String word, int node_id, int sender_id, List<URLResponse> response) {
		this();
		this.word = word;
		this.node_id = node_id;
		this.sender_id = sender_id;
		this.response = response;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new SearchResponseMessProc(this);
	}

	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public int getSender_id() {
		return sender_id;
	}
	public void setSender_id(int sender_id) {
		this.sender_id = sender_id;
	}
	public List<URLResponse> getResponse() {
		return response;
	}
	public void setResponse(List<URLResponse> response) {
		this.response = response;
	}
}
