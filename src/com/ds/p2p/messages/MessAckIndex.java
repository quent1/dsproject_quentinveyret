package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.AckIndexMessProc;

public class MessAckIndex extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', identifying the target node
	 */
	private int node_id;
	/**
	 * The keyword from the original INDEX message
	 */
	private String keyword;

	public MessAckIndex() {
		this.type = MessageType.ACK_INDEX;
	}
	public MessAckIndex(int node_id, String keyword) {
		this();
		this.node_id = node_id;
		this.keyword = keyword;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new AckIndexMessProc(this);
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
