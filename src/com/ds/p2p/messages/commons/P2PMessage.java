package com.ds.p2p.messages.commons;

import com.ds.p2p.messages.processors.MessageProcessor;

public abstract class P2PMessage {
	protected MessageType type;
	public MessageType getType() {
		return type;
	}
	public abstract MessageProcessor getMessageProcessor();
}
