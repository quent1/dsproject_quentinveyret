package com.ds.p2p.messages.commons;

public class URLResponse {

	private String url;
	private int rank;

	public URLResponse(String url, int rank) {
		this.url = url;
		this.rank = rank;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
}
