package com.ds.p2p.messages.commons;

public class Node {

	private int node_id;
	private String ip_address;
	private int port;

	public Node(int node_id, String ip_address, int port) {
		this.node_id = node_id;
		this.ip_address = ip_address;
		this.port = port;
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
