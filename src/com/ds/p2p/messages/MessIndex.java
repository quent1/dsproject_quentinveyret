package com.ds.p2p.messages;

import java.util.ArrayList;
import java.util.List;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.IndexMessProc;

public class MessIndex extends P2PMessage {

	/**
	 * The target ID
	 */
	private int target_id;
	/**
	 * A non-negative number of order 2'^32^', of this message originator
	 */
	private int sender_id;
	/**
	 * The word being indexed
	 */
	private String keyword;
	private List<String> link = new ArrayList<String>();

	public MessIndex() {
		type = MessageType.INDEX;
	}
	public MessIndex(int target_id, int sender_id, String keyword, List<String> link) {
		this();
		this.target_id = target_id;
		this.sender_id = sender_id;
		this.keyword = keyword;
		this.link = link;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new IndexMessProc(this);
	}

	public int getTarget_id() {
		return target_id;
	}
	public void setTarget_id(int target_id) {
		this.target_id = target_id;
	}
	public int getSender_id() {
		return sender_id;
	}
	public void setSender_id(int sender_id) {
		this.sender_id = sender_id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public List<String> getLink() {
		return link;
	}
	public void setLink(List<String> link) {
		this.link = link;
	}
}
