package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.SearchMessProc;

public class MessSearch extends P2PMessage {

	/**
	 * The word to search for
	 */
	private String word;
	/**
	 * Target node ID
	 */
	private int node_id;
	/**
	 * A non-negative number of order 2'^32^', of this message originator
	 */
	private int sender_id;

	public MessSearch() {
		type = MessageType.SEARCH;
	}
	public MessSearch(String word, int node_id, int sender_id) {
		this();
		this.word = word;
		this.node_id = node_id;
		this.sender_id = sender_id;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new SearchMessProc(this);
	}

	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public int getSender_id() {
		return sender_id;
	}
	public void setSender_id(int sender_id) {
		this.sender_id = sender_id;
	}
}
