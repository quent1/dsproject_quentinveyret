package com.ds.p2p.messages;

import com.ds.p2p.messages.commons.MessageType;
import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.messages.processors.impl.AckMessProc;

public class MessAck extends P2PMessage {

	/**
	 * A non-negative number of order 2'^32^', identifying the suspected dead node
	 */
	private int node_id;
	/**
	 * The ip address of sending node, this changes on each hop
	 */
	private String ip_address;

	public MessAck() {
		type = MessageType.ACK;
	}
	public MessAck(int node_id, String ip_address) {
		this();
		this.node_id = node_id;
		this.ip_address = ip_address;
	}

	@Override
	public MessageProcessor getMessageProcessor() {
		return new AckMessProc(this);
	}

	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
}
