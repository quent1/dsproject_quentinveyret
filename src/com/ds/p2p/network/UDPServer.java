package com.ds.p2p.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.StringTokenizer;

import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.messages.processors.MessageProcessor;
import com.ds.p2p.util.JsonConverter;

public class UDPServer extends Thread {

	private DatagramSocket socket;

	public UDPServer(DatagramSocket socket) throws SocketException {
		this.socket = socket;
	}

	@Override
	public void run() {
		System.out.println("UDP server running on port " + socket.getLocalPort());
		byte[] receiveData = new byte[1024];
		while (true) {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            try {
				socket.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
            StringTokenizer sentence = new StringTokenizer(new String(receivePacket.getData()), "\r\n");
            String json = sentence.nextToken();
            System.out.println("RECEIVED: " + json);
            P2PMessage mess = JsonConverter.convert(json);
            MessageProcessor processor = mess.getMessageProcessor();
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            processor.process(IPAddress.getHostAddress(), port);
		}
	}
}
