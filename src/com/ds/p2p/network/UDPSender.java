package com.ds.p2p.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

import com.ds.p2p.messages.commons.P2PMessage;
import com.ds.p2p.util.JsonConverter;
import com.ds.p2p.util.NodeConfig;

public class UDPSender extends Thread {

	private P2PMessage message;
	private InetSocketAddress targetIp;

	public UDPSender(P2PMessage message, InetSocketAddress targetIp) {
		this.message = message;
		this.targetIp = new InetSocketAddress(targetIp.getAddress(), NodeConfig.getPort());//targetIp;
	}

	@Override
	public void run() {
		DatagramSocket clientSocket = null;
		try {
			clientSocket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		String mess = JsonConverter.convert(message) + "\r\n";
		byte[] sendData = mess.getBytes();
		System.out.println("To be send: \"" + mess + "\"");
		System.out.println("Message of " + sendData.length + " bytes sent to " + targetIp.getAddress().getHostAddress() + ":" + targetIp.getPort());
		DatagramPacket sendPacket = null;
		try {
			sendPacket = new DatagramPacket(sendData, sendData.length, targetIp);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	    try {
			clientSocket.send(sendPacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
