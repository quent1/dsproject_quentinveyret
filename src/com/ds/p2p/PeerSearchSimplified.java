package com.ds.p2p;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public interface PeerSearchSimplified {
	
	/**
	 * Initialize with a udp socket
	 * @param udp_socket
	 */
	void init(DatagramSocket udp_socket);
	/**
	 * returns network_id, a locally generated number to identify peer network
	 * @param bootstrap_node
	 * @param identifier
	 * @param target_identifier
	 * @return
	 */
	long joinNetwork(InetSocketAddress bootstrap_node, String identifier, String target_identifier );
	/**
	 * parameter is previously returned peer network identifier
	 * @param network_id
	 * @return
	 */
	boolean leaveNetwork(long network_id);
	void indexPage(String url, String[] unique_words);
	SearchResult[] search(String[] words);
}
