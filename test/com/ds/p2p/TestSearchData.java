package com.ds.p2p;

import org.junit.Test;

import com.ds.p2p.messages.commons.Node;

public class TestSearchData {

	@Test
	public void test() {
		SearchData.insertNode(new Node(15, "192.168.1.1", 8767));
		SearchData.insertNode(new Node(16, "192.168.1.2", 8767));
		SearchData.insertNode(new Node(17, "192.168.1.3", 8767));
		SearchData.insertNode(new Node(18, "192.168.1.4", 8767));
		SearchData.insertNode(new Node(18, "192.168.1.5", 8767));

		Node nearest = SearchData.getNearestNode(20);
		System.out.println(nearest.getNode_id() + " | " + nearest.getIp_address());
		
		SearchData.removeNode(15);
		
		nearest = SearchData.getNearestNode(10);
		System.out.println(nearest.getNode_id());

		/*
		String[] words = {"coucou", "heho"};
		SearchData.indexPage("coucou.fr", words);
		SearchData.indexPage("hey.fr", words);*/
	}

	@Test
	public void testSearch() {
		/*SearchData.indexPage("blabla.com", new String[] {"word", "bla"});
		SearchData.indexPage("d8.tv", new String[] {"apple"});
		SearchData.indexPage("xx.com", new String[] {"apple", "word", "bla"});
		SearchData.indexPage("fr.fr", new String[] {"bla"});*/

		SearchResult[] results = SearchData.search(new String[] {"bla", "word", "oucou"});

		System.out.println("-------------------");
		for (SearchResult result : results) {
			System.out.println(result.url + ":");
			for (String word : result.words) {
				System.out.println("- \"" + word + "\"");
			}
			System.out.println("Frequency: " + result.frequency);
			System.out.println("-------------------");
		}
	}

}
