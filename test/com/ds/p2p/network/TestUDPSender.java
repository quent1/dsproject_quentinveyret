package com.ds.p2p.network;

import java.net.InetSocketAddress;

import org.junit.Test;

import com.ds.p2p.messages.MessLeavingNetwork;
import com.ds.p2p.util.NodeConfig;

public class TestUDPSender {

	@Test
	public void test() {
		UDPSender us = new UDPSender(new MessLeavingNetwork(25), new InetSocketAddress("127.0.1.5", NodeConfig.getPort()));
		us.run();
	}

}
