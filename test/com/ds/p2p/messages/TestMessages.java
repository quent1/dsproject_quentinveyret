package com.ds.p2p.messages;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestMessAck.class, TestMessIndex.class,
		TestMessJoiningNetwork.class, TestMessJoiningNetworkRelay.class,
		TestMessLeavingNetwork.class, TestMessPing.class,
		TestMessRoutingInfo.class, TestMessSearch.class, TestMessSearchResponse.class })
public class TestMessages {

}
