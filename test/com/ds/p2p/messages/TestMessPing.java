package com.ds.p2p.messages;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessPing {

	@Test
	public void test() {
		MessPing mp = new MessPing(23, 56, "199.1.5.4");
		Gson gson = new Gson();
		String json = gson.toJson(mp, MessPing.class);
		System.out.println(json);
		assertEquals(json, "{\"target_id\":23,\"sender_id\":56,\"ip_address\":\"199.1.5.4\",\"type\":\"PING\"}");
	}

}
