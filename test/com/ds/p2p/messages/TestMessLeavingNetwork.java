package com.ds.p2p.messages;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessLeavingNetwork {

	@Test
	public void test() {
		MessLeavingNetwork mln = new MessLeavingNetwork(42);
		Gson gson = new Gson();
		String json = gson.toJson(mln, MessLeavingNetwork.class);
		System.out.println(json);
		assertEquals(json, "{\"node_id\":42,\"type\":\"LEAVING_NETWORK\"}");
	}

}
