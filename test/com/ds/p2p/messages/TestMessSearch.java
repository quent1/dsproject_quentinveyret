package com.ds.p2p.messages;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessSearch {

	@Test
	public void test() {
		MessSearch ms = new MessSearch("apple", 34, 34);
		Gson gson = new Gson();
		String json = gson.toJson(ms, MessSearch.class);
		System.out.println(json);
		assertEquals(json, "{\"word\":\"apple\",\"node_id\":34,\"sender_id\":34,\"type\":\"SEARCH\"}");
	}

}
