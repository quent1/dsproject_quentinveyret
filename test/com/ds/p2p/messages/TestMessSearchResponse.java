package com.ds.p2p.messages;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.ds.p2p.messages.commons.URLResponse;
import com.google.gson.Gson;

public class TestMessSearchResponse {

	@Test
	public void test() {
		List<URLResponse> response = new ArrayList<URLResponse>();
		response.add(new URLResponse("www.dsg.cs.tcd.ie/", 32));
		response.add(new URLResponse("www.scss.tcd.ie/courses/mscnds/", 1));
		MessSearchResponse msr = new MessSearchResponse("word", 45, 34, response);
		Gson gson = new Gson();
		String json = gson.toJson(msr, MessSearchResponse.class);
		assertEquals(json, "{\"word\":\"word\",\"node_id\":45,\"sender_id\":34,\"response\":[{\"url\":\"www.dsg.cs.tcd.ie/\",\"rank\":32},{\"url\":\"www.scss.tcd.ie/courses/mscnds/\",\"rank\":1}],\"type\":\"SEARCH_RESPONSE\"}");
	}

}
