package com.ds.p2p.messages;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.ds.p2p.messages.commons.Node;
import com.google.gson.Gson;

public class TestMessRoutingInfo {

	@Test
	public void test() {
		List<Node> route_table = new ArrayList<Node>();
		route_table.add(new Node(3, "199.1.5.3", 8767));
		route_table.add(new Node(22, "199.1.5.4", 8767));
		MessRoutingInfo mri = new MessRoutingInfo(34, 42, "199.1.5.2", route_table);
		Gson gson = new Gson();
		String json = gson.toJson(mri, MessRoutingInfo.class);
		System.out.println(json);
		assertEquals(json, "{\"gateway_id\":34,\"node_id\":42,\"ip_address\":\"199.1.5.2\",\"route_table\":[{\"node_id\":3,\"ip_address\":\"199.1.5.3\"},{\"node_id\":22,\"ip_address\":\"199.1.5.4\"}],\"type\":\"ROUTING_INFO\"}");
	}

}
