package com.ds.p2p.messages;

import static org.junit.Assert.*;
import org.junit.Test;

import com.google.gson.Gson;

public class TestMessAck {

	@Test
	public void test() {
		MessAck ma = new MessAck(23, "199.1.5.4");
		Gson gson = new Gson();
		String json = gson.toJson(ma, MessAck.class);
		System.out.println(json);
		assertEquals(json, "{\"node_id\":23,\"ip_address\":\"199.1.5.4\",\"type\":\"ACK\"}");
	}

}
