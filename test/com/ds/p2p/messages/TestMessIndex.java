package com.ds.p2p.messages;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessIndex {

	@Test
	public void test() {
		List<String> link = new ArrayList<String>();
		link.add("http://www.newindex.com");
		link.add("http://www.xyz.com");
		MessIndex mi = new MessIndex(34, 34, "XXX", link);

		Gson gson = new Gson();
		String json = gson.toJson(mi, MessIndex.class);
		System.out.println(json);
		assertEquals(json, "{\"target_id\":34,\"sender_id\":34,\"keyword\":\"XXX\",\"link\":[\"http://www.newindex.com\",\"http://www.xyz.com\"],\"type\":\"INDEX\"}");
	}

}
