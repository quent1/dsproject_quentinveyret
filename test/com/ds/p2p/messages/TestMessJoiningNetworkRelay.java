package com.ds.p2p.messages;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessJoiningNetworkRelay {

	@Test
	public void test() {
		MessJoiningNetworkRelay mjnr = new MessJoiningNetworkRelay(42, 42, 34);
		Gson gson = new Gson();
		String json = gson.toJson(mjnr, MessJoiningNetworkRelay.class);
		System.out.println(json);
		assertEquals(json, "{\"node_id\":42,\"target_id\":42,\"gateway_id\":34,\"type\":\"JOINING_NETWORK_RELAY_SIMPLIFIED\"}");
	}

}
