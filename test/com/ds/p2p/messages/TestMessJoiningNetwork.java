package com.ds.p2p.messages;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

public class TestMessJoiningNetwork {

	@Test
	public void test() {
		MessJoiningNetwork mjn = new MessJoiningNetwork(42, 42, "199.1.5.2");
		Gson gson = new Gson();
		String json = gson.toJson(mjn, MessJoiningNetwork.class);
		System.out.println(json);
		assertEquals(json, "{\"node_id\":42,\"target_id\":42,\"ip_address\":\"199.1.5.2\",\"type\":\"JOINING_NETWORK_SIMPLIFIED\"}");
	}

}
